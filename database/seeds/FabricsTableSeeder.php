<?php

use Illuminate\Database\Seeder;

class FabricsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fabrics = [
            [
                'name' => "Licra",
            ],
            [
                'name' => "Algodon",
            ],
            [
                'name' => "Lino",
            ],
            [
                'name' => 'Terciopelo'
            ]
        ];

        DB::table('fabrics')->insert($fabrics);
    }
}
