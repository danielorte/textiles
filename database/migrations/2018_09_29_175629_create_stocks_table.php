<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fabric_id');
            $table->unsignedInteger('fabric_model_id')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->unsignedInteger('color_id')->nullable();
            $table->integer('roll_quantity')->nullable();
            $table->boolean('is_roll')->default(false);
            $table->decimal('quantity',10,2);
            $table->decimal('remaining_quantity',10,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
