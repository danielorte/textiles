<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFabricModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fabric_models', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku')->nullable();
            $table->unsignedInteger('fabric_id');
            $table->unsignedInteger('category_id')->nullable();
            $table->unsignedInteger('color_id')->nullable();
            $table->string('image')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fabric_models');
    }
}
