<?php

namespace App;

use App\Stock;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fabric extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'price'
    ];

    public function models()
    {
        return $this->hasMany('App\FabricModel');
    }

    public function categories()
    {
        return $this->hasMany('App\FabricCategory');
    }

    public function colors()
    {
        return $this->hasMany('App\FabricColor');
    }

    public function stock()
    {
        return $this->hasMany('App\Stock')->orderBy('fabric_model_id');
    }

    public function currentStock()
    {
        return $this->hasOne('App\Stock')->selectRaw('fabric_id,SUM(remaining_quantity) as total')->groupBy('fabric_id');
    }
}
