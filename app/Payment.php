<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'sale_id','amount',
    ];

    public function sale()
    {
        return $this->belongsTo('App\Sale')->withTrashed();
    }
}
