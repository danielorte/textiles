<?php

namespace App\Http\Controllers;

use App\Fabric;
use App\Color;
use App\FabricColor;
use Illuminate\Http\Request;

class FabricColorController extends Controller
{
    public function store(Fabric $fabric, Color $color)
    {
        $fabricColor = FabricColor::create([
            'fabric_id' => $fabric->id,
            'color_id' => $color->id,
        ]);

        return response()->json([
            'status' => (bool) $fabricColor,
            'data'   => $fabricColor,
            'message' => $fabricColor ? 'Color Added!' : 'Error Adding Color'
        ]);
    }
    public function destroy(FabricColor $fabColor)
    {
        $status = $fabColor->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Color Deleted!' : 'Error Deleting Color'
        ]);
    }
}
