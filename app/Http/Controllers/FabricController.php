<?php

namespace App\Http\Controllers;

use Storage;
use App\Fabric;
use App\FabricModel;
use App\Http\Resources\FabricCollection;
use Illuminate\Http\Request;

class FabricController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new FabricCollection(Fabric::with(['currentStock'])->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fabric = Fabric::create([
            'name' => $request->name,
            'price' => $request->price,
        ]);

        $categories = $request->categories;
        if(count($categories) > 0){
            $fabric->categories()->createMany($categories);
        }

        $colors = $request->colors;
        if(count($colors) > 0){
            $fabric->colors()->createMany($colors);
        }

        $models = $request->models;
        $stock = $request->stock;
        $hasStock = count($stock);

        if(count($models) > 0){
            foreach ($models as $key => $value) {
                $modelId = $value['id'];
                $model = FabricModel::create([
                    'sku' => $value['sku'],
                    'fabric_id' => $fabric->id,
                    'category_id' => isset($value['category_id']) ? $value['category_id'] : null,
                    'color_id' => isset($value['color_id']) ? $value['color_id'] : null,
                ]);
                if($hasStock > 0){
                    $validId = $this->searchStockToAdd($modelId,$stock);
                    if($validId >= 0){
                        $stock[$validId]['fabric_model_id'] = $model->id;
                    }
                }
                if(isset($value['image'])){
                    $file_data = $value['image'];
                    $file_name = $model->id.'_'.time().'.png'; //generating unique file name; 
                    list($type, $file_data) = explode(';', $file_data);
                    list(, $file_data) = explode(',', $file_data); 
                    if($file_data!=""){ // storing image in storage/app/public Folder 
                        Storage::disk('public')->put('\\fabrics\\'.$file_name,base64_decode($file_data));
                        $model->image = '\\storage\\fabrics\\'.$file_name;
                        $model->save();
                    }
                }
            }
        }
        if($hasStock > 0){
            $fabric->stock()->createMany($stock);
        }
        return response()->json([
            'status' => (bool) $fabric,
            'data'   => $fabric,
            'message' => $fabric ? 'Fabric Created!' : 'Error Creating Fabric'
        ]);
    }

    public function searchStockToAdd($id,$stock){
        $exists = -1;
        foreach ($stock as $key => $value) {
            if($value['fabric_model_id'] == $id && $exists < 0){
                $exists = $key;
            }
        }
        return $exists;
    }

    public function search(Request $request)
    {
        $query_result = Fabric::selectRaw('DISTINCT(fabrics.id)')
            ->leftJoin('fabric_models','fabric_models.fabric_id','=','fabrics.id');

        if (isset($request->name)) {
            $query_result->where('name', 'like', '%'.$request->name.'%')
            ->orWhere('sku', 'like', '%'.$request->name.'%');
        }
        $result = $query_result->get();

        $fabrics = Fabric::with([
            'categories.category','colors.color','stock.model.category','stock.model.color',
            'models.fabric','models.category','models.color','models.currentStock'
        ])->whereIn('fabrics.id',$result)->get();

        return response()->json($fabrics,200);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Fabric  $fabric
     * @return \Illuminate\Http\Response
     */
    public function show(Fabric $fabric)
    {
        return response()->json(Fabric::with([
            'categories.category','colors.color','stock.model.category','stock.model.color',
            'models.category','models.color'
        ])->find($fabric->id),200); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fabric  $fabric
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fabric $fabric)
    {
        $status = $fabric->update(
            $request->only(['name','price'])
        );

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Fabric Updated!' : 'Error Updating Fabric'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fabric  $fabric
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fabric $fabric)
    {
        $status = $fabric->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Fabric Deleted!' : 'Error Deleting Fabric'
        ]);
    }
}
