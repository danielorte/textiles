<?php

namespace App\Http\Controllers;

use App\Fabric;
use App\Category;
use App\FabricCategory;
use Illuminate\Http\Request;

class FabricCategoryController extends Controller
{
    public function store(Fabric $fabric, Category $category)
    {
        $fabricCategory = FabricCategory::create([
            'fabric_id' => $fabric->id,
            'category_id' => $category->id,
        ]);

        return response()->json([
            'status' => (bool) $fabricCategory,
            'data'   => $fabricCategory,
            'message' => $fabricCategory ? 'Category Added!' : 'Error Adding Category'
        ]);
    }
    public function destroy(FabricCategory $fabcatid)
    {
        $status = $fabcatid->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Category Deleted!' : 'Error Deleting Category'
        ]);
    }
}
