<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Customer::orderBy('name')->get(),200);
    }

    public function balance(Request $request)
    {
        $paginate = 15;
        $debtors = Customer::selectRaw('DISTINCT(customers.id)')
        ->join('sales','sales.customer_id','customers.id')
        ->where('sales.balance','>',0)
        ->where('sales.deleted_at',NULL)->get();
        $results = Customer::with(['currentBalance'])->whereIn('customers.id', $debtors);
        if(isset($request->id) && !is_null($request->id)){
            $results->where('customers.id',$request->id);
        }
        if(isset($request->per_page)){
            $paginate = $request->per_page;
        }
        $results->orderBy('customers.name');
        return response()->json($results->paginate($paginate),200);
    }

    public function details(Customer $customer)
    {
        $customer = Customer::with([
            'sales.articles.model.fabric','sales.articles.model.category',
            'sales.articles.model.color','sales.payments','currentBalance',
            'sales.customer'
        ])->find($customer->id);
        return response()->json($customer,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = Customer::create([
            'name' => $request->name,
            'address' => $request->address,
            'telephone' => $request->telephone,
            'mobilephone' => $request->mobilephone,
            'email' => $request->email,
            'credit_days' => $request->credit_days,
            'comments' => $request->comments,
        ]);

        return response()->json([
            'status' => (bool) $customer,
            'data'   => $customer,
            'message' => $customer ? 'Customer Created!' : 'Error Creating Customer'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        return response()->json($customer,200); 
    }

    public function search(Request $request)
    {
        $query_result = Customer::select('*')->where('deleted_at',NULL);

        if (isset($request->name)) {
            $query_result->where('name', 'like', '%'.$request->name.'%');
        }
        $customers = $query_result->get();

        return response()->json($customers,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $status = $customer->update(
            $request->only(['name','address','telephone','mobilephone','email','credit_days','comments'])
        );

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Customer Updated!' : 'Error Updating Customer'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $status = $customer->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Customer Deleted!' : 'Error Deleting Customer'
        ]);
    }
}
