<?php

namespace App\Http\Controllers;

use App\Color;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Color::all(),200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $color = Color::create([
            'name' => $request->name,
            'value' => $request->value,
        ]);

        return response()->json([
            'status' => (bool) $color,
            'data'   => $color,
            'message' => $color ? 'Color Created!' : 'Error Creating Color'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function show(Color $color)
    {
        return response()->json($color,200); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Color $color)
    {
        $status = $color->update(
            $request->only(['name','value'])
        );

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Color Updated!' : 'Error Updating Color'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function destroy(Color $color)
    {
        $status = $color->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Color Deleted!' : 'Error Deleting Color'
        ]);
    }
}
