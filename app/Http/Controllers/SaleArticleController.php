<?php

namespace App\Http\Controllers;

use App\SaleArticle;
use Illuminate\Http\Request;

class SaleArticleController extends Controller
{
    public function store(Request $request)
    {
        $saleArticle = SaleArticle::create([
            'sale_id' => $request->sale_id,
            'fabric_model_id' => $request->fabric_model_id,
            'description' => $request->description,
            'quantity' => $request->quantity,
            'price' => $request->price,
            'total' => $request->total,
        ]);

        return response()->json([
            'status' => (bool) $saleArticle,
            'data'   => $saleArticle,
            'message' => $saleArticle ? 'Article Added!' : 'Error Adding Article'
        ]);
    }
    public function destroy(SaleArticle $article)
    {
        $status = $article->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Article Deleted!' : 'Error Deleting Article'
        ]);
    }
}
