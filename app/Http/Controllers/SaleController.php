<?php

namespace App\Http\Controllers;

use PDF;
use DB;
use App\Stock;
use App\Sale;
use App\Customer;
use App\NumALetra;
use App\Payment;
use App\Mail\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $paginate = 15;
        $results = Sale::with(['customer']);
        $ids = Sale::select('id');
        if(isset($request->id)){
            $results->where('id','like','%'.$request->id.'%');
            $ids->where('id','like','%'.$request->id.'%');
        }
        if(isset($request->start_date)){
            $results->where('created_at','>=',$request->start_date.' 00:00:00');
            $ids->where('created_at','>=',$request->start_date.' 00:00:00');
        }
        if(isset($request->end_date)){
            $results->where('created_at','<=',$request->end_date.' 23:59:59');
            $ids->where('created_at','<=',$request->end_date.' 23:59:59');
        }
        if(isset($request->customer_id) && !is_null($request->customer_id)){
            $results->where('customer_id',$request->customer_id);
            $ids->where('customer_id',$request->customer_id);
        }
        if(isset($request->per_page)){
            $paginate = $request->per_page;
        }
        $results->orderBy('sales.created_at','DESC');
        $ids->orderBy('sales.created_at','DESC');
        if($request->customer_id != 9) $ids->where('customer_id','!=',9);
        $sales = $results->paginate($paginate);
        $ids->limit($paginate);
        $totals = collect([
            'totals' => Sale::selectRaw('
            sum(revenue) as revenue,sum(total) as total,sum(balance) as balance
            ')
            ->whereIn('id',$ids->get())->first(),
        ]);
        $data = $totals->merge($sales);
        return response()->json($data,200);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(count($request->articles) > 0){
            $articles_error = [];
            DB::beginTransaction();
            foreach ($request->articles as $value) {
                if(isset($value['fabric_model_id']) && !is_null($value['fabric_model_id'])){
                    $stocks = Stock::where('fabric_model_id',$value['fabric_model_id'])
                        ->where('remaining_quantity', '>' ,0)->get();
                    $stock = Stock::where('fabric_model_id',$value['fabric_model_id'])->sum('remaining_quantity');
                    if($value['quantity'] > $stock){
                        $value['current_stock'] = $stock;
                        $articles_error[] = $value;
                    } else {
                        $quantity = $value['quantity'];
                        foreach ($stocks as $model) {
                            if($quantity > 0){
                                if($quantity > $model['remaining_quantity']){
                                    $quantity -= $model['remaining_quantity'];
                                    $model['remaining_quantity'] = 0;
                                }
                                else{
                                    $model['remaining_quantity'] -= $quantity;
                                }
                                $model->save();
                            }
                        }
                    }
                }
            }
            if(count($articles_error) > 0){
                DB::rollBack();
                return response()->json([
                    'status' => false,
                    'data'   => $articles_error,
                    'message' => 'Not enough stock for some models',
                    'code' => 201
                ],500);
            }
            else DB::commit();
        }

        $sale = Sale::create([
            'customer_id' => $request->customer_id,
            'balance' => 0,
            'total' => 0,
        ]);
        
        if(isset($request->customer) && !isset($request->customer_id) 
            && !is_null($request->customer['name'])){
            $customer = Customer::create(['name' => $request->customer['name']]);
            $sale->customer_id = $customer->id;    
        }

        $articles = $request->articles;
        if(count($articles) > 0){
            $sale->articles()->createMany($articles);
        }

        $sale->total = $sale->articles()->sum('total');
        $sale->revenue = $sale->articles()->sum('revenue');
        if($request->balance > 0){
            $sale->balance = $sale->total;
            if($request->payment > 0){
                $payment = Payment::create([
                    'sale_id' => $sale->id,
                    'amount' => $request->payment,
                ]);
                $sale->balance -= $request->payment;
            }
        }
        $sale->save();

        return response()->json([
            'status' => (bool) $sale,
            'data'   => $sale,
            'message' => $sale ? 'Sale Created!' : 'Error Creating Sale'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show(Sale $sale)
    {
        $sale = Sale::with([
            'articles.model.fabric','articles.model.category','articles.model.color','customer','payments'
        ])->find($sale->id);

        return response()->json($sale,200); 
    }

    public function pdf(Sale $sale) {
        $sale = Sale::with([
            'articles.model.fabric','articles.model.category','articles.model.color','customer','payments'
        ])->find($sale->id);
        
        $sale->total_amount = NumALetra::numeroALetra($sale->total);

        $sale->due_date = date("d/m/Y",strtotime($sale->created_at));
        if(!is_null($sale->customer) && !is_null($sale->customer->credit_days)) {
            $sale->due_date=date("d/m/Y",strtotime($sale->created_at.' + '.$sale->customer->credit_days.' day'));
        }
        $sale->dia = date("d",strtotime($sale->created_at));
        $sale->mes = NumALetra::obtenerMesConLetra($sale->created_at);
        $sale->anio = date("Y",strtotime($sale->created_at));
        
        $pdf = PDF::loadView('invoice',['data' => $sale]);
        return $pdf->stream('');
    }
    
    public function sendInvoice(Request $request, Sale $sale)
    {
        //return new Invoice($sale);
        $recipients = $request->recipients;
        $pdf = $this->pdf($sale);
        $mail = Mail::to($recipients[0]);
        if(count($recipients) > 1){
            unset($recipients[0]);
            $mail->cc($recipients);
        }
        $mail->send(new Invoice($sale, $pdf));

        $status = count(Mail::failures()) == 0;
        return response()->json([
            'status' => $status,
            'data' => Mail::failures(),
            'message' => $status ? 'Invoice Sended!' : 'Error Sending Invoice'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {
        $status = $sale->update(
            $request->only(['customer_id'])
        );

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Sale Updated!' : 'Error Updating Sale'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {
        $sale = Sale::with(['articles.model'])->find($sale->id);
        $stock = [];
        $articles = $sale->articles;
        foreach ($articles as $value) {
            if(isset($value['fabric_model_id']) && !is_null($value['fabric_model_id'])){
                $stock[] = [
                    'fabric_model_id' => $value->fabric_model_id,
                    'fabric_id' => $value->model->fabric_id,
                    'roll_quantity' => 1,
                    'is_roll' => 1,
                    'quantity' => $value->quantity,
                    'remaining_quantity' => $value->quantity,
                ];
            }
        }
        
        if(count($stock) > 0){
            Stock::insert($stock);
        }

        $status = $sale->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Sale Deleted!' : 'Error Deleting Sale'
        ]);
    }
}
