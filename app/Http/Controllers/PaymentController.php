<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function store(Request $request)
    {
        $payment = Payment::create([
            'sale_id' => $request->sale_id,
            'amount' => $request->amount,
        ]);

        $payment->sale->balance -= $payment->amount;
        $payment->sale->save();

        return response()->json([
            'status' => (bool) $payment,
            'data'   => $payment,
            'message' => $payment ? 'Payment Added!' : 'Error Adding Payment'
        ]);
    }

    public function destroy(Payment $payment)
    {

        $payment->sale->balance += $payment->amount;
        $payment->sale->save();

        $status = $payment->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Payment Deleted!' : 'Error Deleting Payment'
        ]);
    }
}
