<?php

namespace App\Http\Controllers;

use Storage;
use App\FabricModel;
use Illuminate\Http\Request;

class FabricModelController extends Controller
{
    public function store(Request $request)
    {
        $fabricModel = FabricModel::create([
            'sku' => $request->sku,
            'fabric_id' => $request->fabric_id,
            'color_id' => $request->color_id,
            'category_id' => $request->category_id,
        ]);
        
        if(isset($request->image)){
            $file_data = $request->image;
            $file_name = $fabricModel->id.'_'.time().'.png'; //generating unique file name; 
            list($type, $file_data) = explode(';', $file_data);
            list(, $file_data) = explode(',', $file_data); 
            if($file_data!=""){ // storing image in storage/app/public Folder 
                Storage::disk('public')->put('\\fabrics\\'.$file_name,base64_decode($file_data));
                $fabricModel->image = '\\storage\\fabrics\\'.$file_name;
                $fabricModel->save();
            }
        }

        return response()->json([
            'status' => (bool) $fabricModel,
            'data'   => $fabricModel,
            'message' => $fabricModel ? 'Model Added!' : 'Error Adding Model'
        ]);
    }

    public function storeImage(FabricModel $fabModel, Request $request){
        if(isset($request->image)){
            $path = '\\storage\\fabrics\\';
            $file = $request->image->store('fabrics','public');
            if((bool) $file){
                $fabModel->image = '/storage/'.$file;
                $fabModel->save();
            }
            /*$file_data = $request->image;
            $file_name = $fabModel->id.'_'.time().'.png'; //generating unique file name; 
            list($type, $file_data) = explode(';', $file_data);
            list(, $file_data) = explode(',', $file_data); 
            if($file_data!=""){ // storing image in storage/app/public Folder 
                Storage::disk('public')->put('\\fabrics\\'.$file_name,base64_decode($file_data));
                $fabModel->image = '\\storage\\fabrics\\'.$file_name;
                $fabModel->save();
            }*/
        }
        return response()->json([
            'status' => (bool) $fabModel,
            'data'   => $fabModel,
            'message' => $fabModel ? 'Image Added!' : 'Error Adding Image'
        ]);
    }
    
    public function destroyImage(FabricModel $fabModel){
        $file_name = str_replace('/storage',"",$fabModel->image);
        $deleted = Storage::disk('public')->delete($file_name);
        if($deleted){
            $fabModel->image = null;
            $fabModel->save();
        }
        return response()->json([
            'status' => (bool) $deleted,
            'data'   => $file_name,
            'message' => $deleted ? 'Image Deleted!' : 'Error Deleting Image'
        ]);
    }  

    public function destroy(FabricModel $fabModel)
    {
        $status = $fabModel->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Model Deleted!' : 'Error Deleting Model'
        ]);
    }
}
