<?php

namespace App\Http\Controllers;

use App\Stock;
use Illuminate\Http\Request;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Stock::with([
            'model.fabric','model.color','model.category'
        ])->get(),200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*if(isset($request->roll_quantity)){
            $stock = [];
            for ($i=0; $i < $request->roll_quantity; $i++) { 
                $stock[] = Stock::create([
                    'name' => $request->name,
                    'fabric_id' => $request->fabric_id,
                    'color_id' => $request->category_id,
                    'category_id' => $request->color_id,
                    'is_roll' => $request->is_roll,
                    'quantity' => $request->quantity,
                    'remaining_quantity' => $request->quantity,
                ]);
            }
            return response()->json([
                'status' => (bool) $stock,
                'data'   => $stock,
                'message' => $stock ? 'Stock Created!' : 'Error Creating Stock'
            ]);
        }
        else {*/
            $stock = Stock::create([
                'name' => $request->name,
                'fabric_model_id' => $request->fabric_model_id,
                'fabric_id' => $request->fabric_id,
                'color_id' => $request->color_id,
                'category_id' => $request->category_id,
                'roll_quantity' => $request->roll_quantity,
                'is_roll' => $request->is_roll,
                'quantity' => $request->quantity,
                'remaining_quantity' => $request->quantity,
            ]);

            return response()->json([
                'status' => (bool) $stock,
                'data'   => $stock,
                'message' => $stock ? 'Stock Created!' : 'Error Creating Stock'
            ]);
        //}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function show(Stock $stock)
    {
        return response()->json($stock,200); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stock $stock)
    {
        $status = $stock->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Stock Deleted!' : 'Error Deleting Stock'
        ]);
    }
}
