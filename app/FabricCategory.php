<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FabricCategory extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'fabric_id','category_id'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category')->withTrashed();
    }
}
