<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaleArticle extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'sale_id','fabric_model_id','description','quantity','price','total','revenue'
    ];

    public function sale()
    {
        return $this->belongsTo('App\Sale')->withTrashed();
    }

    public function model()
    {
        return $this->belongsTo('App\FabricModel','fabric_model_id')->withTrashed();
    }
}
