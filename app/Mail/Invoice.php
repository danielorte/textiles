<?php

namespace App\Mail;

use PDF;
use App\Sale;
use App\NumALetra;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Invoice extends Mailable
{
    use Queueable, SerializesModels;

    protected $sale;
    protected $pdf;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Sale $sale, $pdf)
    {
        $this->sale = $sale;
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('invoice_email',['data' => $this->sale])
            ->subject('Venta '.$this->sale->id)
            ->attachData($this->pdf, 'Venta '.$this->sale->id.'.pdf', ['mime' => 'application/pdf',]);
    }
}
