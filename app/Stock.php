<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'fabric_model_id','fabric_id','category_id','color_id','roll_quantity','is_roll','quantity','remaining_quantity'
    ];

    public function model()
    {
        return $this->belongsTo('App\FabricModel','fabric_model_id')->withTrashed();
    }

    public function fabric()
    {
        return $this->belongsTo('App\Fabric')->withTrashed();
    }

    public function category()
    {
        return $this->belongsTo('App\Category')->withTrashed();
    }

    public function color()
    {
        return $this->belongsTo('App\Color')->withTrashed();
    }
}
