<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FabricModel extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    
    protected $fillable = [
        'sku','fabric_id','category_id','color_id','image'
    ];

    public function fabric()
    {
        return $this->belongsTo('App\Fabric')->withTrashed();
    }

    public function category()
    {
        return $this->belongsTo('App\Category')->withTrashed();
    }

    public function color()
    {
        return $this->belongsTo('App\Color')->withTrashed();
    }
    
    public function currentStock()
    {
        return $this->hasOne('App\Stock')->selectRaw('fabric_model_id,SUM(remaining_quantity) as total')->groupBy('fabric_model_id');
    }
}
