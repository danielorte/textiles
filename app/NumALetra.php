<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NumALetra extends Model
{
	public static function Unidades($num)
	{
	    switch($num)
	    {
	        case 1: return "UN";
	        case 2: return "DOS";
	        case 3: return "TRES";
	        case 4: return "CUATRO";
	        case 5: return "CINCO";
	        case 6: return "SEIS";
	        case 7: return "SIETE";
	        case 8: return "OCHO";
	        case 9: return "NUEVE";
	    }

	    return "";
	}

	public static function Decenas($num)
	{

	    $decena = floor($num/10);
	    $unidad = $num - ($decena * 10);

	    switch($decena)
	    {
	        case 1:
	            switch($unidad)
	            {
	                case 0: return "DIEZ";
	                case 1: return "ONCE";
	                case 2: return "DOCE";
	                case 3: return "TRECE";
	                case 4: return "CATORCE";
	                case 5: return "QUINCE";
	                default: return "DIECI".NumALetra::Unidades($unidad);
	            }
	        case 2:
	            switch($unidad)
	            {
	                case 0: return "VEINTE";
	                default: return "VEINTI".NumALetra::Unidades($unidad);
	            }
	        case 3: return NumALetra::DecenasY("TREINTA", $unidad);
	        case 4: return NumALetra::DecenasY("CUARENTA", $unidad);
	        case 5: return NumALetra::DecenasY("CINCUENTA", $unidad);
	        case 6: return NumALetra::DecenasY("SESENTA", $unidad);
	        case 7: return NumALetra::DecenasY("SETENTA", $unidad);
	        case 8: return NumALetra::DecenasY("OCHENTA", $unidad);
	        case 9: return NumALetra::DecenasY("NOVENTA", $unidad);
	        case 0: return NumALetra::Unidades($unidad);
	    }
	}

	public static function DecenasY($strSin, $numUnidades)
	{
	    if ($numUnidades > 0)
	    return $strSin." Y ".NumALetra::Unidades($numUnidades);

	    return $strSin;
	}

	public static function Centenas($num)
	{
	    $centenas = floor($num / 100);
	    $decenas = $num - ($centenas * 100);

	    switch($centenas)
	    {
	        case 1:
	            if ($decenas > 0)
	                return "CIENTO ".NumALetra::Decenas($decenas);
	            return "CIEN";
	        case 2: return "DOSCIENTOS ".NumALetra::Decenas($decenas);
	        case 3: return "TRESCIENTOS ".NumALetra::Decenas($decenas);
	        case 4: return "CUATROCIENTOS ".NumALetra::Decenas($decenas);
	        case 5: return "QUINIENTOS ".NumALetra::Decenas($decenas);
	        case 6: return "SEISCIENTOS ".NumALetra::Decenas($decenas);
	        case 7: return "SETECIENTOS ".NumALetra::Decenas($decenas);
	        case 8: return "OCHOCIENTOS ".NumALetra::Decenas($decenas);
	        case 9: return "NOVECIENTOS ".NumALetra::Decenas($decenas);
	    }

	    return NumALetra::Decenas($decenas);
	}

	public static function Seccion($num, $divisor, $strSingular, $strPlural)
	{
	    $cientos = floor($num / $divisor);
	    $resto = $num - ($cientos * $divisor);

	    $letras = "";

	    if ($cientos > 0)
	        if ($cientos > 1)
	            $letras = NumALetra::Centenas($cientos)." ".$strPlural;
	        else
	            $letras = $strSingular;

	    if ($resto > 0)
	        $letras .= "";

	    return $letras;
	}

	public static function Miles($num)
	{
	    $divisor = 1000;
	    $cientos = floor($num / $divisor);
	    $resto = $num - ($cientos * $divisor);

	    $strMiles = NumALetra::Seccion($num, $divisor, "UN MIL", "MIL");
	    $strCentenas = NumALetra::Centenas($resto);

	    if($strMiles == "")
	        return $strCentenas;

	    return $strMiles." ".$strCentenas;
	}

	public static function Millones($num)
	{
	    $divisor = 1000000;
	    $cientos = floor($num / $divisor);
	    $resto = $num - ($cientos * $divisor);

	    if($num%$divisor == 0)
	        $strMillones = NumALetra::Seccion($num, $divisor, "UN MILLON DE", "MILLONES DE");
	    else
	        $strMillones = NumALetra::Seccion($num, $divisor, "UN MILLON", "MILLONES");
	    
	    $strMiles = NumALetra::Miles($resto);

	    if($strMillones == "")
	        return $strMiles;

	    return $strMillones." ".$strMiles;
	}

	public static function numeroALetra($cantidad, $tipo_cambio=0)
	{
		$cantidad_letra = "";
		$data = [
			'numero' => $cantidad,
			'enteros' => floor($cantidad),
			'centavos' => (((round($cantidad * 100)) - (floor($cantidad) * 100))),
			'letrasCentavos' => "",
			'letrasMonedaPlural' => "PESOS",
			'letrasMonedaSingular' => "PESO",
			'letrasMonedaCentavo' => "/100"
		];
		if($tipo_cambio==0){
			$data['letrasMonedaPlural'] = "PESOS";
			$data['letrasMonedaSingular'] = "PESO";
		}
	    $data['letrasCentavos'] = $data['centavos'].$data['letrasMonedaCentavo'];
		if($data['enteros'] == 0)
        	$cantidad_letra .= "CERO ".$data['letrasMonedaPlural']." ".$data['letrasCentavos'];
        else if($data['enteros'] == 1)
        	$cantidad_letra .= NumALetra::Millones($data['enteros'])." ".$data['letrasMonedaSingular']." ".$data['letrasCentavos'];
        else
        	$cantidad_letra .= NumALetra::Millones($data['enteros'])." ".$data['letrasMonedaPlural']." ".$data['letrasCentavos'];
		if($tipo_cambio==0)
			$cantidad_letra.=" M.N";
		else
			$cantidad_letra.=" M.N";
		return $cantidad_letra;
	}
	
	public static function obtenerFechaEnLetra($fecha)
	{
		$dia= NumALetra::conocerDiaSemanaFecha($fecha);
		$num = date("j", strtotime($fecha));
		$anno = date("Y", strtotime($fecha));
		$mes = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
		$mes = $mes[(date('m', strtotime($fecha))*1)-1];
		return $dia.' '.$num.' de '.$mes.' de '.$anno;
	}
	
	public static function conocerDiaSemanaFecha($fecha)
	{
		$dias = array('domingo', 'lunes', 'martes', 'miÃ©rcoles', 'jueves', 'viernes', 'sÃ¡bado');
		$dia = $dias[date('w', strtotime($fecha))];
		return $dia;
	}

	public static function obtenerFechaConMesEnLetra($fecha)
	{
		$num = date("j", strtotime($fecha));
		$anno = date("Y", strtotime($fecha));
		$mes = array('ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE');
		$mes = $mes[(date('m', strtotime($fecha))*1)-1];
		return $num.'/'.$mes.'/'.$anno;
	}

	public static function obtenerMesConLetra($fecha)
	{
		$mes = array('ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE');
		$mes = $mes[(date('m', strtotime($fecha))*1)-1];
		return $mes;
	}
}