<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');

Route::get('/fabrics', 'FabricController@index');
Route::get('/fabrics/search', 'FabricController@search');
Route::get('/fabrics/{fabric}', 'FabricController@show');

Route::get('/categories', 'CategoryController@index');
Route::get('/categories/{category}', 'CategoryController@show');

Route::get('/colors', 'ColorController@index');
Route::get('/colors/{color}', 'ColorController@show');

Route::group(['middleware' => 'auth:api'], function(){
    Route::get('/users','UserController@index');
    Route::get('users/{user}','UserController@show');
    Route::patch('users/{user}','UserController@update');
    Route::resource('/categories', 'CategoryController')->except(['index','show']);
    Route::resource('/colors', 'ColorController')->except(['index','show']);

    Route::resource('/fabrics', 'FabricController')->except(['index','show']);

    Route::resource('/stock', 'StockController');
    Route::get('/customers/search', 'CustomerController@search');
    Route::get('/customers/balance', 'CustomerController@balance');
    Route::get('/customers/{customer}/details', 'CustomerController@details');
    Route::resource('/customers', 'CustomerController');

    Route::post('/sales/{sale}/payment', 'PaymentController@store');
    Route::delete('/payments/{payment}', 'PaymentController@destroy');

    Route::get('/sales/{sale}/pdf', 'SaleController@pdf');
    Route::post('/sales/{sale}/email', 'SaleController@sendInvoice');
    Route::resource('/sales', 'SaleController');


    Route::post('/fabrics/{fabric}/categories/{category}', 'FabricCategoryController@store');
    Route::delete('/fabric_category/{fabcatid}', 'FabricCategoryController@destroy');

    Route::post('/fabrics/{fabric}/colors/{color}', 'FabricColorController@store');
    Route::delete('/fabric_color/{fabColor}', 'FabricColorController@destroy');

    Route::delete('/fabric_color/{fabColor}', 'FabricColorController@destroy');

    Route::post('/fabric_models', 'FabricModelController@store');
    Route::delete('/fabric_models/{fabModel}', 'FabricModelController@destroy');
    Route::post('/fabric_models/{fabModel}/image', 'FabricModelController@storeImage');
    Route::delete('/fabric_models/{fabModel}/image', 'FabricModelController@destroyImage');

    Route::post('/sale_articles', 'SaleArticleController@store');
    Route::delete('/sale_articles/{article}', 'SaleArticleController@destroy');

});