<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Folio {{ $data->id }}</title>
	<link rel="stylesheet" type="text/css" href="{{ public_path('css/bootstrap.css') }}" media="all">
	<link rel="stylesheet" type="text/css" href="{{ public_path('css/factura.css') }}" media="all">
</head>
<body>
	<div class="original">
		<div>
			<div class="row">
				<div class="pull-left">
					<img src="{{ $message->embed(public_path('/img/logo-144.png')) }}" style="width: 140px;margin: 4px 16px">
				</div>
				<div class="pull-right" style="margin-right:20px;">
                    <h4>TEXTILES HERNANDEZ</h4>
                    <p>ventas@textileshernandez.com</p>
                    <p>Juan de Dios Robledo #1027</p>
                    <p>36657476</p>
				</div>
            </div>
        </div>
		<div class="container-fluid">
			<div class="row">
				<div class="col col-xs-12">
					<div class="col-heading text-uppercase">Datos de cliente</div>
					<div class="col-content text-uppercase">
                        @if($data->customer)
                        <p><b>{{ $data->customer->name }}</b></p>
                        @else
                        <p><b>N/A</b></p>
                        @endif
					</div>
				</div>
				<div class="col col-xs-12">
                    <div class="col-heading">Nota de Venta</div>
                    <div class="col col-xs-5 text-right text-uppercase font-10">
                        <b>Folio</b><br>
                        <b>Fecha y Hora</b><br>
                    </div>
                    <div class="col col-xs-7 pull-right font-10">
                        <div style="margin-left: 8px">
                            <b>{{ $data->id }}</b><br>
                            {{ $data->created_at }}
                        </div>
                    </div>
				</div>
            </div>
			<div class="row">
				<table class="table main-table table-bordered">
					<thead class="font-10">
						<tr>
							<th>Cantidad</th>
							<th style="width: 205px">Articulo</th>
							<th>Valor Unitario</th>
							<th>Importe</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($data->articles as $concepto)
						<tr>
							<td>{{ number_format($concepto->quantity, 2, '.', ',') }}</td>
							<td>
								@if (!is_null($concepto->model))
									{{ $concepto->model->fabric->name }}
									{{ $concepto->model->sku }}
									@if(!is_null($concepto->model->category))
									{{ $concepto->model->category->name }}
									@endif
									@if(!is_null($concepto->model->color))
									{{ $concepto->model->color->name }}
									@endif
								@else
									{{ $concepto->description}}
								@endif
							</td>
							<td>${{ number_format($concepto->price, 2, '.', ',') }}</td>
							<td>${{ number_format($concepto->total, 2, '.', ',') }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
            </div>
			<div class="row">
				<div class="col col-xs-7">
					<div class="col-heading font-10">Importe con letra</div>
					<div class="col-content text-center font-10" style="border: 1px solid #000">
						{{ $data->total_amount }}
					</div>
				</div>
				<div class="col col-xs-5">
                    <div class="col col-xs-7 text-right">
                        <b>Total:</b><br>
                        @if($data->balance > 0)
                        <b>Saldo Pendiente:</b>
                        @endif
                    </div>
                    <div class="col col-xs-4 pull-right text-right">
                        <div style="margin-left: 8px">
                            <div style="border-top: 2px solid #000;border-bottom: 2px solid #000"><b>${{ number_format($data->total, 2, '.', ',') }}</b></div>
                        </div>
                        @if($data->balance > 0)
                        <div style="margin-left: 8px">
                            <div><b>${{ number_format($data->balance, 2, '.', ',') }}</b></div>
                        </div>
                        @endif
                    </div>
				</div>
            </div>
            @if($data->balance > 0)
			<div class="row" style="margin-top: 10px">
				<p class="font-8">
                    Por este pagare reconozco deber y me obligo a pagar incondicionalmente a la orden de TEXTILES HERNANDEZ
                    en la ciudad de Guadalajara, Jalisco o en cualquier otro lugar que se me requiera el pago el día
                    {{ $data->due_date }} la cantidad de {{ number_format($data->total, 2, '.', ',') }} 
                    ({{$data->total_amount}}) Valor recibido en MERCANCIA a mi entera satisfacción. Este pagare es mercantil
                    y está regido por la Ley General de Títulos Y Operaciones de Crédito en su artículo 173 parte final y 
                    artículos correlativos  por no ser un pagare domiciliado. De no verificarse el pago de la cantidad que
                    este expresa el día de su vencimiento, abonare al rédito del 5 % (cinco por ciento mensual) por todo 
                    el tiempo que este insoluto sin perjuicio al cobro mas por los gastos que por ello se originen . 
                    @if($data->customer)
                        {{ $data->customer->name }}, Domicilio: {{ $data->customer->address }}
                    @endif
                </p>
            </div>
			@endif
            @if(count($data->payments) > 0)
			<div class="row">
				<table class="table main-table table-bordered">
					<thead class="font-10">
						<tr>
							<th colspan="3" class="text-center">Abonos</th>
						</tr>
						<tr>
							<th>#</th>
							<th>Fecha</th>
							<th>Cantidad</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($data->payments as $key => $concepto)
						<tr>
							<td>{{ $key+1 }}</td>
							<td>{{ date("d/m/Y H:i:s",strtotime($concepto->created_at)) }}</td>
							<td>${{ number_format($concepto->amount, 2, '.', ',') }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			@endif
        </div>
    </div>
    <div class="my-footer"><img src="{{ $message->embed(public_path('/img/logo-184.png')) }}"></div>
	@if($data->balance > 0)
    <div class="saltopagina"></div>
	<div class="container-fluid contrato">
		<div class="row">
			<div class="col col-xs-12">
				<div class="col-content font-11 text-justify">
					<p>
						<b>CONTRATO DE COMPRAVENTA QUE CELEBRAN POR UNA PARTE TEXTILES HERNANDEZ, POR SU PROPIO DERECHO, A QUIEN EN LO SUCESIVO SE LE DENOMINARA COMO EL “VENDEDOR”, Y POR LA OTRA 
                        @if($data->customer)                        
                        <u class="text-uppercase">{{ $data->customer->name }}</u> A QUIEN EN LO SUCESIVO SE LE DENOMINARA  COMO 
                        @endif
                        EL “CLIENTE”, A QUIENES DE MANERA CONJUNTA SE LES DENOMINARÁ COMO “LAS PARTES”. AL TENOR DE LAS SIGUIENTES:</b>
					</p>
					<p class="text-center"><b>CLÁUSULAS</b></p>
          			<p><b>PRIMERA.- CONSENTIMIENTO Y OBJETO.-</b> Las partes manifiestan su voluntad para celebrar el presente Contrato de compraventa de la Mercancía descritos en la Nota de Remisión que forma parte integral del presente; por lo que el VENDEDOR se obliga a vender y entregar al CLIENTE dicha Mercancía en los términos establecidos en este Contrato; y el CLIENTE  en consecuencia, se obliga a pagar como contraprestación el importe mencionado en la Nota de Remisión.</p> 
          			<p><b>SEGUNDA.- COSTO Y FORMA DE PAGO.-</b> El costo que el CLIENTE deberá de pagar al VENDEDOR objeto de la venta de la Mercancía, es el señalado en la nota de remisión. La forma de pago se realizará en efectivo, cheque de caja, transferencia electrónica o tarjeta de crédito, en moneda nacional o dólares americanos.  El VENDEDOR entregará al CLIENTE la factura, recibo o comprobante de entrega en el que constarán los datos específicos de la compraventa realizada y/o el importe y concepto de los pagos que reciba por parte del CLIENTE junto con la entrega del Material solicitado. El CLIENTE efectuará el pago como contraprestación en el domicilio del VENDEDOR señalado en el presente Contrato, a más tardar a los {{ $data->customer && !is_null($data->customer->credit_days) ? $data->customer->credit_days : '1' }} días naturales posteriores a la entrega del Material. 
          			Intereses moratorios.- Las partes acuerdan que la falta de pago descrita en la nota de remisión en la fecha que corresponda obligarán al CLIENTE a pagar intereses moratorios  del 6% seis por ciento mensual sobre la cantidad total del adeudo, desde la fecha de incumplimiento y hasta la fecha en que el pago se realice. </p>
          			<p><b>TERCERA.- ENTREGA DE LA MERCANCÍA.-</b> La entrega de la Mercancía se realizará en el domicilio del VENDEDOR señalado en la Nota de Remisión, salvo que el CLIENTE por escrito solicite al VENDEDOR que la entrega se haga en el domicilio que señale el CLIENTE. En ese caso el VENDEDOR deberá señalar en el presupuesto del costo correspondiente por concepto de maniobras, fletes y seguros. </p>
          			<p><b>CUARTA.- OBLIGACIONES DE LAS PARTES:</b><br> 
          			<b>OBLIGACIONES DEL VENDEDOR:</b> a) El VENDEDOR se obliga al cumplimiento de lo establecido en el presente Contrato. b) Expedir factura o comprobante fiscal, por el o los pagos realizados por el CLIENTE. c) Responder por los defectos o vicios ocultos de la Mercancía que los hagan impropios para el uso a que habitualmente se destinan o que disminuyan su calidad y especificaciones de uso. d) El VENDEDOR se hace responsable de los daños materiales imputables a él, o a sus empleados, que con motivo de la entrega de la Mercancía pudieran ocasionarse. f) Otorgar la Garantía establecida en el presente Contrato.</p>
				</div>
			</div>
			<div class="col col-xs-12">
				<div class="col-content font-11 text-justify">
					<p><b>OBLIGACIONES DEL CLIENTE:</b> a) El CLIENTE se obliga al cumplimiento de lo establecido en el presente Contrato. b) Realizar el pago señalado en el presente Contrato. c) Recibir la Mercancía en el horario establecido por las partes señalando al VENDEDOR el personal autorizado para recibirlos.  </p>
					<p><b>QUINTA.- GARANTÍA.-</b> El CLIENTE cuenta con 30 treinta días naturales a partir de la entrega de la Mercancía objeto de la compraventa para realizar cualquier reclamación. Toda reclamación dentro del término de la garantía deberá ser realizada directamente en el establecimiento del VENDEDOR estipulado en el presente Contrato. En caso de que la Garantía sea otorgada, el VENDEDOR deberá de indicarle al CLIENTE el procedimiento a seguir para  el cumplimiento de la Garantía. </p>
          			<p><b>SEXTA.- CAUSAS DE RESCISIÓN.-</b> Son causas de rescisión: a) Que Las partes no cumplan con lo estipulado en el presente Contrato. b) Que la Mercancía no se entreguen en la fecha o forma convenida. d) La falta de pago por parte del CLIENTE conforme a lo establecido en el presente Contrato. En caso de rescisión del presente Contrato, la parte que incumpla deberá de pagar lo correspondiente a la pena convencional. </p>
          			<p><b>SEPTIMA.- PENA CONVENCIONAL.-</b> En caso de incumplimiento por alguna de las partes a las obligaciones establecidas en el presente Contrato, la parte que incumpla se hará acreedora a la pena convencional correspondiente del 25% veinticinco cinco por ciento del importe correspondiente a  la compraventa de la Mercancía. Esta sanción se estipula por el simple retraso en el incumplimiento de las obligaciones por causas imputables a las partes, sin perjuicio del derecho que tienen de optar entre exigir el cumplimiento del Contrato o rescindirlo. </p>
          			<p><b>OCTAVA.- JURISDICCIÓN.-</b> Para todo lo relacionado al cumplimiento y ejecución del presente Contrato las Partes acuerdan someterse a la legislación aplicable en los Estados Unidos Mexicanos y a la jurisdicción de los Tribunales del Primer Partido Judicial de Guadalajara, Jalisco, renunciando expresamente a cualquier otro fuero que legalmente o por cualquier otra circunstancia pudiera corresponderles en el presente o en lo futuro.</p>
          			<p>Leído que fue por las partes el contenido del presente contrato y sabedoras de su alcance legal, lo firman por duplicado en la Ciudad de <u>Guadalajara</u> a los <u>{{ $data->dia }}</u> días del mes de <u>{{ $data->mes }}</u> del año <u>{{ $data->anio }}</u>.<br><br>
          			EL VENDEDOR<br><br>
          			_________________________________________________<br>
          			En Representación de Textiles Hernandez. <br><br>
          			EL CLIENTE<br><br>
					_________________________________________________<br>
					Nombre<br><br>
					_________________________________________________<br>
					Firma
          			</p>
				</div>
			</div>
		</div>
	</div>
	@endif
</body>
</html>