<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}">
        <title>Textiles Hernández</title>
        <link rel="apple-touch-icon" sizes="57x57" href="/img/logo/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/img/logo/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/img/logo/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/img/logo/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/img/logo/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/img/logo/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/img/logo/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/img/logo/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/img/logo/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/img/logo/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/img/logo/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/img/logo/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/img/logo/favicon-16x16.png">
        <link rel="manifest" href="/img/logo/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/img/logo/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <link href=" {{ mix('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <app></app>
        </div>
        <script src="{{ mix('js/bootstrap.js') }}"></script>
        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>