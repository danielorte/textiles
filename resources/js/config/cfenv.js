import environment from './env.json';

const router = [];
const menu = [];

function initRouter(router, menu, envMenu){
    envMenu.forEach(element => {
        if(element.componentPath)
            element.component = require("../"+(element.componentPath));
        if(element.children){
            element.children.forEach(children => {
                if(children.componentPath) {
                    children.component = require("../"+(children.componentPath));
                }
            });
        }
        router.push(element);
        if(element.inMenu){
            let item = {
                name: element.inMenu.displayName,
            };
            if(element.children) {
                item.show = false;
                item.to = { href:element.inMenu.id };
                item.children = [];
                element.children.forEach(children => {
                    if(children.inMenu){
                        item.children.push({
                            name: children.inMenu.displayName,
                            to: { path: element.path +'/'+ children.path }
                        });
                    }
                });
            }
            else {
                item.to = { path: element.path };
            }
            menu.push(item);
        }
    });
}

initRouter(router, menu, environment.menu);

const cfenv = {
    getByName(name){
        return environment[name] || null;
    },
    getMainMenu(){
        return menu;
    },
    getRoutes(){
        return router;
    }
}

export default cfenv;